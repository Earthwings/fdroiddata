Categories:Wallpaper
License:GPLv3+
Web Site:https://github.com/glesik/wpgen/blob/HEAD/README.md
Source Code:https://github.com/glesik/wpgen
Issue Tracker:https://github.com/glesik/wpgen/issues
Changelog:https://github.com/glesik/wpgen/releases
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8RNEWF8QHQHTN
Bitcoin:16qE3ae8GmSwwaVodXyu76iAThR7GtyrTW

Auto Name:WPGen
Summary:Wallpaper generator
Description:
Android app which can generate solid color or diagonal gradient multicolor
wallpapers. You can choose from a number of pre-defined colors or add your
own.
.

Repo Type:git
Repo:https://github.com/glesik/wpgen.git

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
