Categories:Reading
License:MIT
Web Site:
Source Code:https://github.com/raulhaag/MiMangaNu
Issue Tracker:https://github.com/raulhaag/MiMangaNu/issues
Changelog:https://github.com/raulhaag/MiMangaNu/blob/HEAD/README.md

Auto Name:Mi Manga Nu
Summary:Manga reader
Description:
Read and organize mangas.
.

Repo Type:git
Repo:https://github.com/raulhaag/MiMangaNu

Build:1.0,1
    disable=force push by upstream
    commit=e59f73b592cb24127c8dcb5e0780a9ec61425780
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.03,3
    commit=80e127a678f6d48b090d5918e03961466fe5d9bc
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.06,6
    commit=95d27bf735e9c0329711991f31942d5940d0fc99
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.07,7
    commit=d9284b1acffa7a005cd713f2973650b15aabae18
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.08,8
    commit=9f3135ddc34f5b1703266b5e03ac45ecb51835f0
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.09,9
    commit=52b202ff12e7502e75cba78da851b55b00069470
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.10,10
    commit=e5891dbe5d4c8024d87a3841266c2ef66bc8b730
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.11,11
    commit=c18f83c59c68c1fceb4e7f59a433375b059f05b2
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.12,12
    commit=912544c76ad85373e249e0c5d8043b9276e5d4a0
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.13,13
    commit=7acdcb78171073f274aa1e1c14c3f07af1c21e28
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.14,14
    commit=0f1cc5f1ad3ab418e4ccdd53b8c0dacb00165767
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.15,15
    commit=bdef42d1b3b21a14f47671673d00a7daffeb83fb
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.16,16
    commit=47bbc9bf86f7d04039fa723acec9053176ad6283
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.17,17
    commit=0825bbf43e28af1418859490fa6d9afebcbabd4f
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.18,18
    commit=51d2fb4057120b9afb5a9789b5df88875cc2116b
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.19,19
    commit=8861a73294cf43ba8c7c0f9260f90d916aa5cbb1
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.20,20
    commit=36de4f88ab0db7b1cbbb894d527dd9c87853b2c7
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.21,21
    commit=cf588e31812364c91de9445b043c72b4c64f23f5
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.22,22
    commit=c22251fd0d732bec613c71c71b8d463467bbfb0d
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.23,23
    disable=imagezoom issues, use included source?
    commit=13a44900f20ea0e74c8ed4ba56c87f39b81000b0
    subdir=MiMangaNu
    gradle=yes
    rm=TransitionEverywhere,MiMangaNu/bin,MiMangaNu/libs,cardview,floating_action_button,ImageViewTouchLibrary
    prebuild=sed -i -e '/compile fileTree/d' -e '/compile project/d' -e "s/apply plugin: 'android'/apply plugin: 'com.android.application'/g" -e '/dependencies/irepositories {\njcenter\(\)\n}\n' -e '/dependencies/acompile "com.android.support:support-v4:21.0.+"\ncompile "com.android.support:appcompat-v7:21.0.+"\ncompile "com.android.support:cardview-v7:21.0.+"\ncompile "com.android.support:recyclerview-v7:21.0.+"\ncompile "it.sephiroth.android.library.imagezoom:imagezoom:2.1.1"\ncompile "com.melnykov:floatingactionbutton:1.3.0"\ncompile "com.shamanland:fab:0.0.8"\ncompile "com.github.andkulikov:transitions-everywhere:1.3.2"\n' build.gradle && \
        sed -i -e '/apply plugin/ibuildscript {\nrepositories {\nmavenCentral\(\)\n}\ndependencies {\nclasspath "com.android.tools.build:gradle:1.0.0"\n}\n}' build.gradle && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/servers/ItNineMangaCom.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/servers/ItNineMangaCom.java && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/servers/SubManga.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/servers/SubManga.java && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/servers/StarkanaCom.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/servers/StarkanaCom.java && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/servers/HeavenMangaCom.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/servers/HeavenMangaCom.java && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/services/DescargaIndividual.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/services/DescargaIndividual.java && \
        iconv -f iso-8859-1 -t utf-8 src/ar/rulosoft/mimanganu/servers/EsMangaOnline.java > utf8 && \
        mv utf8 src/ar/rulosoft/mimanganu/servers/EsMangaOnline.java

Build:1.24,24
    commit=a7285c617f2b3e1ab3db456bb8d1d1825c307db1
    subdir=app
    gradle=yes

Build:1.25,25
    commit=b3337dd8212c6f4488bf11f632f3265253aaa4db
    subdir=app
    gradle=yes

Build:1.26,26
    commit=242c6908cddad6c3988c43742c80aada2d64a183
    subdir=app
    gradle=yes

Build:1.27,27
    commit=819d1bd5f16cb161493156a9055df04fba741212
    subdir=app
    gradle=yes

Build:1.28,28
    commit=3b2dc52d770e2d59e431958301037bdbf4ece89e
    subdir=app
    gradle=yes

Build:1.29,29
    commit=d75d5ff846095f58f31d57d99e1ad9a95f434ee2
    subdir=app
    gradle=yes

Build:1.30,30
    commit=c950fd65ab10e12f1763bf2c96a11044744201b5
    subdir=app
    gradle=yes

Build:1.31,31
    commit=7579aa3ebea516ee263529b2acb77f1d36689563
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.31
Current Version Code:31

