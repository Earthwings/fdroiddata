Categories:Navigation
License:GPLv2
Web Site:http://www.navit-project.org
Source Code:http://sourceforge.net/p/navit/code
Issue Tracker:http://trac.navit-project.org

Name:Navit
Summary:Car navigation system
Description:
Its modular design is capable of using vector maps of various formats
for routing and rendering of the displayed map. It's even possible to
use multiple maps at a time. While Navit supports all major platforms,
there is a fork specializing on an easy-to-use, Android-only solution:
[[com.zoffcc.applications.zanavi]].

The routing engine not only calculates an optimal route to your
destination, but also generates directions and even speaks to you.

Navit currently speaks 27 languages.
You can help translating via the web-based
[http://translations.launchpad.net/navit/trunk/+pots/navit translation page].
.

#http://wiki.navit-project.org/index.php/Android_development
Repo Type:git-svn
Repo:https://svn.code.sf.net/p/navit/code/trunk

Build:5650,5650
    commit=5650
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5720,5720
    commit=5720
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5827,5827
    commit=5827
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5830,5830
    commit=5830
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:5889,5889
    commit=5889
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        ln -s android-build/navit/android/* .
    novcheck=yes

Build:6010,6010
    commit=6010
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6012,6012
    commit=6012
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6017,6017
    commit=6017
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6042,6042
    commit=6042
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6045,6045
    commit=6045
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6056,6056
    commit=6056
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6066,6066
    commit=6066
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6068,6068
    commit=6068
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6069,6069
    commit=6069
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml && \
        sed -i -e 's/viena mylia/%d mylia/g' navit/po/lt.po.in
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6077,6077
    commit=6077
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6080,6080
    commit=6080
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6084,6084
    commit=6084
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6085,6085
    commit=6085
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6087,6087
    commit=6087
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6088,6088
    commit=6088
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6089,6089
    commit=6089
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6090,6090
    commit=6090
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6093,6093
    commit=6093
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6095,6095
    commit=6095
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Build:6118,6118
    disable=build fail
    commit=6118
    srclibs=ACRA@acra-4.3.1
    rm=navit/navit/android/libs/acra-4.3.0b2.jar
    prebuild=echo '<project name="Navit"><target name="clean"/></project>' > build.xml
    update=no
    build=export PATH=$PATH:$$SDK$$/tools:$$SDK$$/platform-tools:$$NDK$$/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86/bin && \
        rm build.xml && \
        mkdir android-build && \
        cd android-build/ && \
        cmake -DCMAKE_TOOLCHAIN_FILE=../navit/Toolchain/arm-eabi.cmake -DDISABLE_QT=true ../navit && \
        make && \
        make android_resources && \
        cd .. && \
        rm android-build/navit/android/local.properties && \
        ln -s android-build/navit/android/* . && \
        pushd $$ACRA$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ACRA$$/target/acra-4.3.1.jar libs/
    novcheck=yes

Maintainer Notes:
Found JAR file at navit/navit/android/libs/TTS_library_stub.jar
.

Auto Update Mode:None
Update Check Mode:RepoTrunk
Current Version:6135
Current Version Code:6135

