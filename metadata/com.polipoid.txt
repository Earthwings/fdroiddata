Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/splondike/polipoid
Issue Tracker:https://github.com/splondike/polipoid/issues
Bitcoin:eef5109f143cf9d8c79e01371913c8f7

Auto Name:Polipoid
Summary:Wrapper for the polipo proxy
Description:
A wrapper for the 'polipo' HTTP(S) proxy. Allows for offline browsing via
polipo's cache, and should also speed browsing somewhat. Will be automatically
switched to offline browsing mode when the network is disconnected.
.

Repo Type:git
Repo:https://github.com/splondike/polipoid.git

Build:1.0.1,2
    commit=v1.0.1
    submodules=yes
    init=TOOLCHAIN=/tmp/polipoid-toolchain && \
        $$NDK$$/build/tools/make-standalone-toolchain.sh --platform=android-8 --install-dir=$TOOLCHAIN && \
        cd polipo/ && \
        PATH=$TOOLCHAIN/bin:$PATH CC=arm-linux-androideabi-gcc EXTRA_DEFINES="-U __linux__" make clean polipo && \
        mv polipo ../assets/
    maven=yes

Build:1.1.0,3
    commit=v1.1.0
    submodules=yes
    build=TOOLCHAIN_8=/tmp/polipoid-toolchain-8 && \
        TOOLCHAIN_16=/tmp/polipoid-toolchain-16 && \
        $$NDK$$/build/tools/make-standalone-toolchain.sh --platform=android-8 --arch=arm --install-dir=$TOOLCHAIN_8 && \
        $$NDK$$/build/tools/make-standalone-toolchain.sh --platform=android-16 --arch=arm --install-dir=$TOOLCHAIN_16 && \
        cd polipo/ && \
        PATH=$TOOLCHAIN_8/bin:$PATH CC=arm-linux-androideabi-gcc EXTRA_DEFINES="-U __linux__" make clean polipo && \
        mv polipo ../src/main/assets/polipo && \
        PATH=$TOOLCHAIN_16/bin:$PATH CC=arm-linux-androideabi-gcc EXTRA_DEFINES="-fPIE -U __linux__" LDFLAGS="-fPIE -pie" make clean polipo && \
        mv polipo ../src/main/assets/polipo-pie
    maven=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.0
Current Version Code:3

