Categories:Science & Education
License:GPLv2
Web Site:https://github.com/vanitasvitae/EnigmAndroid/wiki/EnigmAndroid
Source Code:https://github.com/vanitasvitae/EnigmAndroid
Issue Tracker:https://github.com/vanitasvitae/EnigmAndroid/issues

Auto Name:EnigmAndroid
Summary:Simulation of the Enigma Machine
Description:
Simulation of the famous Enigma cipher machine used in the Second World War.

The Enigma Machine was a typewriter-like machine, which used a set of
mechanical Rotors to achieve polyalphabetic substitution.

Features:
* Authentic Rotors
* Ringsettings
* Double Step Anomaly
* Working plugboard

More Information about the historical Enigma can be found on [https://de.wikipedia.org/wiki/Enigma_%28Maschine%29 Wikipedia] (german).
.

Repo Type:git
Repo:https://github.com/vanitasvitae/EnigmAndroid

Build:0.1.1-18.02.2015-beta,6
    commit=707cdfcff66aababce83af1769ba9d8527067c8d
    subdir=app
    gradle=yes

Build:0.1.1-23.02.2015-beta,7
    commit=965aee43dd998c7f0fc70093e53ac05fd7311eab
    subdir=app
    gradle=yes

Build:0.1.2-24.02.2015-beta,8
    commit=310e44029ae1ee3754c630d923cb8f5468ea26d3
    subdir=app
    gradle=yes

Build:0.1.3-14.03.2015-beta,9
    commit=v0.1.3-14.03.2015-beta
    subdir=app
    gradle=yes

Build:0.1.4-15.08.2015-beta,10
    commit=2e40542
    subdir=app
    gradle=yes

Build:0.1.5-27.08.2015-beta,11
    commit=v0.1.5-27.08.2015-beta
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.5-27.08.2015-beta
Current Version Code:11

